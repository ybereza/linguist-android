package com.spotlight.linguist

import android.app.Application
import android.content.res.Resources
import com.spotlight.linguist.ui.main.NavigationRouter

class LinguistApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        res = resources
    }

    override fun onTerminate() {
        super.onTerminate()
        instance = null
        res = null
    }

    companion object {
        var instance: LinguistApp? = null
            private set
        var res: Resources? = null
            private set
    }
}

package com.spotlight.linguist.ui.main

import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import androidx.appcompat.app.AppCompatActivity
import android.transition.AutoTransition
import com.spotlight.linguist.R
import com.spotlight.linguist.ui.lessons.details.CardFragment
import com.spotlight.linguist.ui.lessons.details.LessonDetailFragment
import com.spotlight.linguist.ui.lessons.list.LessonListFragment
import com.spotlight.linguist.ui.settings.SettingsFragment

/**
 * This class must be used to navigate between fragments without direct access to activity
 */

class NavigationRouter (
        val activity: AppCompatActivity
) {
    private var twoPane = false;

    fun setTwoPaneMode(enabled: Boolean) {
        twoPane = enabled
    }

    fun navigateToLesson(lessonId: String) {
        if (twoPane) {
            val arguments = Bundle()
            arguments.putString(CardFragment.ARG_ITEM_ID, lessonId)
            val fragment = CardFragment()
            fragment.arguments = arguments
            activity.supportFragmentManager.beginTransaction()
                    .replace(R.id.lesson_detail_container, fragment).commit()

        } else {
            val bar = activity.supportActionBar
            bar?.setHomeAsUpIndicator(null)

            val arguments = Bundle()
            arguments.putString(LessonDetailFragment.ARG_ITEM_ID, lessonId)
            val fragment = LessonDetailFragment()
            fragment.arguments = arguments

            val transaction = activity.supportFragmentManager.beginTransaction()
            fragment.enterTransition = AutoTransition()
            fragment.exitTransition = AutoTransition()
            transaction.replace(R.id.main_fragment_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    fun navigateToLessonsList() {
        val lessonsList = LessonListFragment()
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.add(R.id.main_fragment_container, lessonsList, LessonListFragment.TAG)
        transaction.commit()
    }

    fun navigateToSettings() {
        val bar = activity.supportActionBar
        bar?.setHomeAsUpIndicator(null)
        val settingsFragment = SettingsFragment()
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_fragment_container, settingsFragment, SettingsFragment.TAG)
        transaction.addToBackStack(SettingsFragment.TAG)
        transaction.commit()
    }
}
package com.spotlight.linguist.actions;

import android.app.Activity;

import com.spotlight.linguist.ui.main.LinguistMainActivity;
import com.spotlight.linguist.R;
import com.spotlight.linguist.actions.base.UIAction;

public class GoToPrefsAction extends UIAction {
    public final static String TAG = GoToPrefsAction.class.getSimpleName();
    public final static int ID = R.id.nav_settings;

    @Override
    public void run(Activity ctx) {
        if (ctx instanceof LinguistMainActivity) {
            ((LinguistMainActivity)ctx).getMainActivityController().goToSettings();
        }
    }
}

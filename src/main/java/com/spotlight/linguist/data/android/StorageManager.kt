package com.spotlight.linguist.data.android

import android.content.Context
import android.os.Bundle
import android.os.Environment
import com.spotlight.linguist.data.StorageManager
import com.spotlight.linguist.system.LifeCycle
import com.spotlight.linguist.system.PermissionProvider
import java.io.File
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AndroidStorageManager(
        context : Context,
        permissionProvider : PermissionProvider,
        private val isExternal : Boolean = false
) : StorageManager, LifeCycle {

    private val LESSONS = "Lessons"

    private val scheduler : ExecutorService = Executors.newFixedThreadPool(1)
    private val permissionProvider = WeakReference<PermissionProvider>(permissionProvider)
    private var started = false

    val internalRootDir : File = context.filesDir
    val externalRootDir : File = context.getExternalFilesDir(null) ?: internalRootDir
    val internalCacheDir : File = context.cacheDir
    val externalCacheDir : File = context.externalCacheDir ?: internalCacheDir

    private val rootDir = if (isExternal) externalRootDir else internalRootDir
    private val cacheDir = if (isExternal) externalCacheDir else internalCacheDir
    private val lessonsDir = File(rootDir, LESSONS)

    enum class STORAGE_TYPE {
        INTERNAL_PRIVATE,
        EXTERNAL_PRIVATE,
        EXTERNAL_PUBLIC,
        UNDEFINED
    }

    data class FileAction(val file: File, val action: (File) -> Unit)

    val actionQueue : LinkedList<FileAction> = LinkedList()

    override fun onCreate(bundle: Bundle?) {
    }

    override fun onStart() {
        started = true
    }

    override fun onStop() {
        started = false
    }

    override fun onResume() {
    }

    override fun onPause() {
    }

    override fun onSaveInstanceState(bundle: Bundle?) {
    }

    override fun onDestroy() {
    }

    override fun root(): File {
        return rootDir
    }

    override fun cacheDir(): File {
        return cacheDir
    }

    override fun lessonsDir(): File {
        return lessonsDir
    }

    private fun putActionInQueue(file: File, action: (File) -> Unit) {
        actionQueue.add(FileAction(file, action))
    }

    override fun performAsyncFileOperation(file: File, action: (File) -> Unit) {
        //if there is no activity attached, store action until any activity attached()
        if (!started) putActionInQueue(file, action)
        else checkAndRun(file, action)
    }

    private fun checkAndRun(file: File, action: (File) -> Unit) {
        when (file.storageTypeOfFile(this)) {
            STORAGE_TYPE.INTERNAL_PRIVATE -> internalAsyncActionRun(file, action)
            else -> requestWritePermissionAndRun(file, action)
        }
    }

    private fun requestWritePermissionAndRun(file: File, action: (File) -> Unit) {

    }

    private fun internalAsyncActionRun(file: File, action: (File) -> Unit) {
        scheduler.submit {
            action(file)
        }
    }
}

internal fun File.storageTypeOfFile(storageManager: AndroidStorageManager) : AndroidStorageManager.STORAGE_TYPE {
    val externalPrivate = storageManager.externalRootDir.absolutePath ?: ""
    val internalPrivate = storageManager.internalRootDir.absolutePath
    val externalPublic = Environment.getExternalStorageDirectory().absolutePath

    return when {
        absolutePath.startsWith(externalPrivate) -> AndroidStorageManager.STORAGE_TYPE.EXTERNAL_PRIVATE
        absolutePath.startsWith(externalPublic) -> AndroidStorageManager.STORAGE_TYPE.EXTERNAL_PUBLIC
        absolutePath.startsWith(internalPrivate) -> AndroidStorageManager.STORAGE_TYPE.INTERNAL_PRIVATE
        else -> AndroidStorageManager.STORAGE_TYPE.UNDEFINED
    }
}

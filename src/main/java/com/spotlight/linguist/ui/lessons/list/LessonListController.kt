package com.spotlight.linguist.ui.lessons.list

import com.spotlight.linguist.data.StorageManager
import com.spotlight.linguist.data.LessonsDataSource
import com.spotlight.linguist.ui.main.NavigationRouter
import java.lang.ref.WeakReference

class LessonListController(
        val router: NavigationRouter,
        val storage : StorageManager) : LessonsDataSource.LessonsDataSourceDelegate {

    private lateinit var lessonListView : WeakReference<LessonListView>
    private var listAdapter : LessonsListAdapter = LessonsListAdapter()

    fun onCreate(view: LessonListView) {
    }

    fun onStart(view : LessonListView) {
        lessonListView = WeakReference(view)
        LessonsDataSource.getInstance().setDelegate(this)
        reloadDataSource()
    }

    fun onStop() {
        LessonsDataSource.getInstance().setDelegate(null)
    }

    fun reloadDataSource() {
        LessonsDataSource.getInstance().update(storage.lessonsDir())
    }

    fun updateData() {
        listAdapter.clear()
        listAdapter.addAll(LessonsDataSource.getInstance().getLessonNames())
        listAdapter.notifyDataSetChanged()
    }

    private fun onListItemClick(position: Int) {
        val lessonName = listAdapter.getLessonName(position)
        router.navigateToLesson(lessonName)
    }

    fun onViewCreated() {
        val lv = lessonListView.get()
        lv?.setAdapter(listAdapter)
        updateData()
    }

    override fun onLessonsDataSourceUpdated(error: LessonsDataSource.LessonsDataSourceError?) {
        if (error == null) {
            updateData()
        } else {
            val view = lessonListView.get();
            view?.showErrorMessage(error.errorDescriptionStringRes());
        }
    }

    override fun onLessonSaveError(error: LessonsDataSource.LessonsDataSourceError?) {
        if (error != null) {
            val view = lessonListView.get();
            view?.showErrorMessage(error.errorDescriptionStringRes());
        }
    }
}
package com.spotlight.linguist.actions.base;

import android.app.Activity;

public abstract class UIAction implements IUIAction {
    public abstract void run(Activity ctx);

    @Override
    public void checkedRun(Activity ctx, Object... objects) throws IncorrectParameterException {
        if (objects != null && objects.length > 0) {
            throw new IncorrectParameterException("UIAction dose not support additional params");
        }
        run(ctx);
    }
}

package com.spotlight.linguist.data;

import android.content.Context;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.AsyncLessonsLoader.LessonsLoaderDelegate;
import com.spotlight.linguist.utils.CollectionUtils;

import org.jetbrains.annotations.Nullable;

public class LessonsDataSource implements LessonsLoaderDelegate {

    public enum LessonsDataSourceError {
        DIRECTORY_NOT_EXISTS {
            @Override
            public int errorDescriptionStringRes() {
                return R.string.directory_not_exist;
            }
        },
        UNKNOWN_ERROR {
            @Override
            public int errorDescriptionStringRes() {
                return R.string.unknown_error;
            }
        },
        FILE_WRITE_ERROR {
            @Override
            public int errorDescriptionStringRes() {
                return R.string.can_not_write_to_file;
            }
        };

        public abstract int errorDescriptionStringRes();
    }

    public interface LessonsDataSourceDelegate {
        void onLessonsDataSourceUpdated(@Nullable LessonsDataSourceError error);
        void onLessonSaveError(@Nullable LessonsDataSourceError error);
    }
    
    private static LessonsDataSource mSelf;
    private int mLastUpdateError = R.string.unknown_error;
    private String mLastUpdateInfo;
    private HashMap<String, Lesson> mLessons = new HashMap<String, Lesson>();
    private LessonsDataSourceDelegate mDelegate;
    
    private LessonsDataSource() {
        
    }
    
    public static LessonsDataSource getInstance() {
        if (mSelf == null) {
            synchronized (LessonsDataSource.class) {
                if (mSelf == null) {
                    mSelf = new LessonsDataSource();
                }
            }
        }
        return mSelf;
    }

    public void setDelegate(LessonsDataSourceDelegate delegate) {
        mDelegate = delegate;
    }

    public void update(String lessonId) {

    }

    public void update(File path) {
        AsyncLessonsLoader loader = new AsyncLessonsLoader(this);
        loader.execute(path);
    }

    public void notifyDataChanged() {
        Collection<Lesson> lessons = mLessons.values();
        boolean modified = false;
        for (Lesson lesson : lessons) {
            if (lesson.isModified()) {
                modified = true;
                File file = new File(lesson.getFilePath());
                if (lesson.getCardsNum() > 0) {
                    try {
                        lesson.saveToFile(file);
                    } catch (IOException e) {
                        mDelegate.onLessonSaveError(LessonsDataSourceError.FILE_WRITE_ERROR);
                    }
                }
                else {
                    file.delete();
                    mLessons.remove(lesson.getLessonName());
                }
            }
        }
        if (modified) mDelegate.onLessonsDataSourceUpdated(null);
    }
    
    public int getLessonsNum() {
        return mLessons.size();
    }

    public Lesson getLessonByName(String lessonName) {
        Lesson lesson = mLessons.get(lessonName);
        return lesson;
    }
    
    public int getLastErrorStringId() {
        return mLastUpdateError;
    }
    
    public String getLastUpdateInfo() {
        return mLastUpdateInfo;
    }
    
    public Collection<String> getLessonNames() {
        if (SettingsManager.sortMethod() == SettingsManager.LessonSortMethod.BY_NAME) {
            ArrayList<String> lessons = new ArrayList<>(mLessons.keySet());
            Collections.sort(lessons);
            return lessons;
        }
        else {
            ArrayList<Lesson> lessons = new ArrayList<Lesson>(mLessons.values());
            Collections.sort(lessons, new Comparator<Lesson>() {
                @Override
                public int compare(Lesson lhs, Lesson rhs) {
                    long diff = lhs.getTimeStamp() - rhs.getTimeStamp();
                    //can not just return diff because diff is long and we need to do reverse sort
                    if (diff > 0)
                        return -1;
                    else if (diff < 0)
                        return 1;
                    else
                        return 0;
                }
            });

            ArrayList<String> out = new ArrayList<String>(lessons.size());
            CollectionUtils.transform(lessons, out, new CollectionUtils.Transformer<Lesson, String>() {
                @Override
                public String transform(Lesson value) {
                    return value.getLessonName();
                }
            });
            return out;
        }
    }
    
    @Override
    public void onLessonsLoaded(List<Lesson> lessons) {
        for (Lesson lesson : lessons) {
            mLessons.put(lesson.getLessonName(), lesson);
        }
        mDelegate.onLessonsDataSourceUpdated(null);
    }

    @Override
    public void onLessonsLoadingError(int resourceStringId, String info) {
        mLastUpdateError = resourceStringId;
        mLastUpdateInfo = info;
        mLessons.clear();
        mDelegate.onLessonsDataSourceUpdated(LessonsDataSourceError.UNKNOWN_ERROR);
    }

    public void removeLesson(String lessonName) {
        Lesson lesson = mLessons.get(lessonName);
        if (lesson != null) {
            mLessons.remove(lessonName);
            File f = new File(lesson.getFilePath());
            if (f.exists()) {
                f.delete();
            }
        }
    }
}

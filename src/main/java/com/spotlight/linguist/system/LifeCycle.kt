package com.spotlight.linguist.system

import android.os.Bundle

interface LifeCycle {
    fun onCreate(bundle : Bundle?)
    fun onStart()
    fun onStop()
    fun onResume()
    fun onPause()
    fun onSaveInstanceState(bundle : Bundle?)
    fun onDestroy()
}
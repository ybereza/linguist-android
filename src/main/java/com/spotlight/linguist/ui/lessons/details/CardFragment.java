package com.spotlight.linguist.ui.lessons.details;

import java.util.ArrayList;
import java.util.List;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.data.Card;
import com.spotlight.linguist.data.Lesson.FileFormatException;
import com.spotlight.linguist.utils.UIUtils;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CardFragment extends Fragment implements OnClickListener, AnimatorListener {

    public static final String ARG_ITEM_ID = "item_id";
    private Card mCard;
    private boolean mRotated;
    
    private ArrayList<View> mCurrentTextViews = new ArrayList<View>();
    private ViewGroup mRootLayout = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle params = getArguments();
        String serialized = params.getString(ARG_ITEM_ID);
        try {
            mCard = Card.createFromString(serialized);
        } catch (FileFormatException e) {
            e.printStackTrace();
        }
    }

    public void updateView(Card card) {
        mCard = card;
        resetState();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.card_layout,
                container, false);
        mRootLayout = (ViewGroup) rootView.findViewById(R.id.cardBackgroundLayout);
        mRootLayout.setOnClickListener(this);

        List<String> words = !SettingsManager.isCardsInverted() ? mCard.getBaseWords() : mCard.getTranslatedWords();
        addSubViews(mRootLayout, words);
        mRotated = false;

        return rootView;
    }
    
    private TextView createNewTextView() {
        TextView textView = new TextView(getActivity());
        textView.setGravity(Gravity.CENTER);
        return textView;
    }
    
    private void addSubViews(ViewGroup layout, List<String> words) {
        mCurrentTextViews.clear();
        /* create a main word with larger font size */
        LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        linearLayout.setLayoutParams(params);
        layout.addView(linearLayout);

        TextView textView = createNewTextView();
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
        textView.setText(words.get(0));
        textView.setTag("text");
        linearLayout.addView(textView);
        mCurrentTextViews.add(textView);
        for (int i = 1; i < words.size(); ++i) {
            /* create a subwords with smaller font size */
            textView = createNewTextView();
            textView.setTextSize(UIUtils.INSTANCE.convertDptoPx(getActivity(), 12));
            textView.setText(words.get(i));
            linearLayout.addView(textView);
            mCurrentTextViews.add(textView);
        }
    }

    @Override
    public void onClick(View v) {
        ViewGroup vg = (ViewGroup)v;
        vg.removeAllViews();
        if (!mRotated) {
            List<String> words = !SettingsManager.isCardsInverted() ?  mCard.getTranslatedWords() : mCard.getBaseWords();
            addSubViews(vg, words);
            
            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(),
                    R.animator.card_flip_left);
            set.setTarget(v);
            set.addListener(this);
            set.start();
        }
        else {
            List<String> words = !SettingsManager.isCardsInverted() ? mCard.getBaseWords() : mCard.getTranslatedWords();
            addSubViews(vg, words);
            
            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(),
                    R.animator.card_flip_right);
            set.setTarget(v);
            set.addListener(this);
            set.start();
        }
        mRotated = !mRotated;
    }

    @Override
    public void onHiddenChanged (boolean hidden) {
        Log.i(this.getTag(), "onHiddenCanged " + hidden);
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        for (View view : mCurrentTextViews) {
            view.setVisibility(View.VISIBLE);
        }        
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void onAnimationStart(Animator animation) {
        for (View view : mCurrentTextViews) {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void resetState() {
        if (mRootLayout != null) {
            mRotated = false;
            mRootLayout.removeAllViews();
            List<String> words = !SettingsManager.isCardsInverted() ? mCard.getBaseWords() : mCard.getTranslatedWords();
            addSubViews(mRootLayout, words);
        }
    }
}

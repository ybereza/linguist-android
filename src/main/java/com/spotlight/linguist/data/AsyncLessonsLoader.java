package com.spotlight.linguist.data;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.Lesson.FileFormatException;


import android.os.AsyncTask;

public class AsyncLessonsLoader extends
        AsyncTask<File, Integer, Boolean> {

    public interface LessonsLoaderDelegate {
        void onLessonsLoaded(List<Lesson> lessons);
        void onLessonsLoadingError(int resourceStringId, String info);
    }
    
    private final ArrayList<Lesson> mLessons = new ArrayList<Lesson>();
    private int mErrorStringId = R.string.unknown_error;
    private LessonsLoaderDelegate mDelegate;
    private String mExceptionInfo;
    
    public AsyncLessonsLoader(LessonsLoaderDelegate delegate) {
        mDelegate = delegate;
    }
    
    @Override
    protected Boolean doInBackground(File... arg0) {
        try {
            final File lessonsDir = arg0[0]; // from where loading lessons
            if (lessonsDir != null && lessonsDir.exists() && lessonsDir.isDirectory()) {
                final File[] files = lessonsDir.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String filename) {
                        if (filename.endsWith(Lesson.FILE_EXTENSION)) {
                            return true;
                        }
                        return false;
                    }
                });
                for(final File file : files) {
                    if (!file.isDirectory()) {
                        mLessons.add(Lesson.createFromFile(file));
                    }
                }
                return true;
            }
            mErrorStringId = R.string.directory_not_exist;
            mExceptionInfo = lessonsDir.getAbsolutePath();
            return false;
        }
        catch(FileFormatException e) {
            mExceptionInfo = e.getMessage();
            mErrorStringId = R.string.error_opening_file;
            return false;
        }
        catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        if (result.booleanValue()) {
            mDelegate.onLessonsLoaded(mLessons);
        }
        else {
            mDelegate.onLessonsLoadingError(mErrorStringId, mExceptionInfo);
        }
        mDelegate = null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        // TODO Auto-generated method stub
        super.onProgressUpdate(values);
    }
    

}

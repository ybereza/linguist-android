package com.spotlight.linguist.system.android

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.spotlight.linguist.system.PermissionProvider
import java.lang.ref.WeakReference
import java.util.*

typealias PermissionCallback = (Boolean) -> Unit

class ActivityPermissionProvider(activity : Activity) : PermissionProvider {

    private val activity = WeakReference<Activity>(activity)
    private val listeners = TreeMap<String, Array<(Boolean) -> Unit>>()
    private val requestCodeConst = 1212

    private val callbacks : Map<String, MutableList<PermissionCallback>> = TreeMap()

    override fun hasPermission(permission: String): Boolean {
        val a = activity.get()
        val granted = a?.let {
            ContextCompat.checkSelfPermission(a, permission) == PackageManager.PERMISSION_GRANTED
        }
        return granted ?: false
    }

    override fun requestPermission(permission: String, result: PermissionCallback) {
        val a = activity.get()
        if (a == null) result(false)

        val tmp = listeners[permission]
        val newPermissions : Array<(Boolean) -> Unit> = tmp?.let {
            arrayOf(result) + it
        } ?: arrayOf(result)
        listeners[permission] = newPermissions

        ActivityCompat.requestPermissions(a!!, arrayOf(permission), requestCodeConst)
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                   grantResults: IntArray) {
        if (requestCode == requestCodeConst) {
            for ((index, permission) in permissions.withIndex()) {
                val tmp : Array<(Boolean) -> Unit>? = listeners.remove(permission)
                tmp?.forEach {
                    it(grantResults[index] == android.content.pm.PackageManager.PERMISSION_GRANTED)
                }
            }
        }
    }
}
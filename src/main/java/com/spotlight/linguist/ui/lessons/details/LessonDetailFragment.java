package com.spotlight.linguist.ui.lessons.details;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.data.Card;
import com.spotlight.linguist.data.Lesson;
import com.spotlight.linguist.data.LessonsDataSource;
import com.spotlight.linguist.data.StorageManager;
import com.spotlight.linguist.data.android.AndroidStorageManager;
import com.spotlight.linguist.system.PermissionProvider;
import com.spotlight.linguist.utils.StringUtils;
import com.spotlight.linguist.utils.UIUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class LessonDetailFragment extends Fragment implements CardSnapHelper.OnSnapListener, PermissionProvider {
    public final static String ARG_ITEM_ID = "item_id";
    
    private CardsAdapter cardsAdapter;
    private RecyclerView cardsList;
    private String lessonName;
    private StorageManager storageManager;

    private interface CardEditCallback {
        void onCardModified(Card card);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        storageManager = new AndroidStorageManager(requireContext(), this, false);
        Bundle arguments = getArguments();
        lessonName = arguments.getString(ARG_ITEM_ID);
        ActionBar bar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if (bar != null) {
            bar.setTitle(lessonName);
        }
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean called = super.onOptionsItemSelected(item);
        if (!called) {
            switch (item.getItemId()) {
                case R.id.card_menu_delete:
                    deleteCard();
                    called = true;
                    break;
                case R.id.card_menu_edit:
                    editCard();
                    called = true;
                    break;
            }
        }
        return called;
    }

    private void addCardToLesson() {
        Card card = new Card();
        editCard(card, new CardEditCallback() {
            @Override
            public void onCardModified(Card card) {
                cardsAdapter.addCard(card);
                LessonsDataSource.getInstance().notifyDataChanged();
            }
        });
    }


    private void editCard() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) cardsList.getLayoutManager();
        final int curItemPos = layoutManager.findFirstCompletelyVisibleItemPosition();
        Card card = cardsAdapter.getCardAtPosition(curItemPos);
        editCard(card, new CardEditCallback() {
            @Override
            public void onCardModified(Card card) {
                cardsAdapter.updateCardAtPos(card, curItemPos);
                cardsAdapter.notifyItemChanged(curItemPos);
                LessonsDataSource.getInstance().notifyDataChanged();
            }
        });
    }

    private void deleteCard() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) cardsList.getLayoutManager();
        int curItemPos = layoutManager.findFirstCompletelyVisibleItemPosition();
        int nextPosition = cardsAdapter.deleteCardAtPosition(curItemPos);
        LessonsDataSource.getInstance().notifyDataChanged();
        if (nextPosition >= 0) {
            cardsList.smoothScrollToPosition(nextPosition);
        }
        else {
            getActivity().onBackPressed(); // no cards, return to lessons list
        }
    }

    @SuppressLint("InflateParams")
    private void editCard(final Card card, final CardEditCallback callback) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(requireContext());
        alertDialog.setTitle(getContext().getResources().getString(R.string.create_new_card));

        final LayoutInflater l = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View v = l.inflate(R.layout.edit_card, null);
        final EditText text = (EditText) v.findViewById(R.id.card_text);
        final EditText translation = (EditText) v.findViewById(R.id.card_translation);
        alertDialog.setView(v);

        List<String> cardText = card.getBaseWords();
        List<String> cardTranslation = card.getTranslatedWords();
        text.setText(StringUtils.join(cardText, Card.STRING_SPLIT_CHAR));
        translation.setText(StringUtils.join(cardTranslation, Card.STRING_SPLIT_CHAR));

        alertDialog.setPositiveButton(com.ybereza.directorybrowser.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String[] cardText = text.getText().toString().split(Card.STRING_SPLIT_CHAR);
                String[] cardTranslation = translation.getText().toString().split(Card.STRING_SPLIT_CHAR);
                card.setBaseWords(Arrays.asList(cardText));
                card.setTranslationWords(Arrays.asList(cardTranslation));
                callback.onCardModified(card);
            }
        });

        alertDialog.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_lesson_detail,
                container, false);
        final LessonsDataSource source = LessonsDataSource.getInstance();
        Lesson lesson = source.getLessonByName(lessonName);
        cardsAdapter = new CardsAdapter();

        if (lesson != null) {
            cardsAdapter.setLesson(SettingsManager.isRandomOrder() ? lesson.shuffle() : lesson);
        }
        else {
            source.setDelegate(new LessonsDataSource.LessonsDataSourceDelegate() {
                @SuppressWarnings("ResourceType")
                @Override
                public void onLessonsDataSourceUpdated(LessonsDataSource.LessonsDataSourceError error) {
                    if (error != null) {
                        String errorMessage = getActivity().getString(error.errorDescriptionStringRes());
                        errorMessage += " " + lessonName;
                        UIUtils.INSTANCE.CreateReportDialog(getActivity(), errorMessage).show();

                    }
                    else {
                        Lesson lesson = source.getLessonByName(lessonName);
                        cardsAdapter.setLesson(SettingsManager.isRandomOrder() ? lesson.shuffle() : lesson);
                    }
                }

                @Override
                public void onLessonSaveError(LessonsDataSource.LessonsDataSourceError error) {
                    UIUtils.INSTANCE.CreateReportDialog(getActivity(), error.errorDescriptionStringRes()).show();
                }
            });
            source.update(storageManager.lessonsDir());
        }

        setupFab(rootView);

        cardsList = (RecyclerView)rootView.findViewById(R.id.pager);
        cardsList.setAdapter(cardsAdapter);

        cardsList.setLayoutManager(new LinearLayoutManager(inflater.getContext(), LinearLayoutManager.HORIZONTAL, false));
        CardSnapHelper snapHelper = new CardSnapHelper(this);
        snapHelper.attachToRecyclerView(cardsList);

        return rootView;
    }

    private void setupFab(View rootView) {
        FloatingActionButton fab = (FloatingActionButton)rootView.findViewById(R.id.fab);
        VectorDrawableCompat icon = VectorDrawableCompat.create(getContext().getResources(),
                R.drawable.ic_add_white_24px, null);
        icon.setTint(Color.WHITE);
        fab.setImageDrawable(icon);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCardToLesson();
            }
        });
    }

    private void setMenuItemIcon(MenuItem item, int resId) {
        if (item != null) {
            VectorDrawableCompat icon = VectorDrawableCompat.create(getContext().getResources(),
                    resId, null);
            if (icon != null) {
                icon.setTint(Color.WHITE);
                item.setIcon(icon);
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.card_main_menu, menu);
        MenuItem item = menu.findItem(R.id.card_menu_edit);
        setMenuItemIcon(item, R.drawable.ic_mode_edit_white_24px);
        item = menu.findItem(R.id.card_menu_delete);
        setMenuItemIcon(item, R.drawable.ic_delete_white_24px);
    }

    @Override
    public boolean hasPermission(@NotNull String permission) {
        return false;
    }

    @Override
    public void requestPermission(@NotNull String permission, @NotNull Function1<? super Boolean, Unit> result) {

    }

    //TODO: Move this code outside fragment!

    @Override
    public void onSnapToPosition(int position) {
        RecyclerView.LayoutManager lm = cardsList.getLayoutManager();
        int prevPosition = position - 1;
        int nextPosition = position + 1;

        if (prevPosition >= 0) {
            View v = lm.findViewByPosition(prevPosition);
            if (v != null) {
                cardsAdapter.resetViewAtPosition(v, prevPosition);
            }
        }

        if (nextPosition < lm.getItemCount()) {
            View v = lm.findViewByPosition(nextPosition);
            if (v != null) {
                cardsAdapter.resetViewAtPosition(v, nextPosition);
            }
        }
    }
}

package com.spotlight.linguist.utils;

import java.util.Collection;
import java.util.Iterator;

public class StringUtils {
    public static String appendIfNotExist(String str, char symbol) {
        char lastsym = str.charAt(str.length()-1);
        if (lastsym != symbol) {
            return str.concat(Character.toString(symbol));
        }
        return str;
    }
    
    public static String removeAtEndIfExist(String str, char symbol) {
        final int len = str.length();
        if (str.charAt(len-1) == symbol) {
            return str.substring(0, len-1);
        }
        return str;
    }
    
    public static String join(Collection<? extends CharSequence> s, String delimiter) {
        int capacity = 0;
        int delimLength = delimiter.length();
        Iterator<? extends CharSequence> iter = s.iterator();
        if (iter.hasNext()) {
            capacity += iter.next().length() + delimLength;
        }

        StringBuilder buffer = new StringBuilder(capacity);
        iter = s.iterator();
        if (iter.hasNext()) {
            buffer.append(iter.next());
            while (iter.hasNext()) {
                buffer.append(delimiter);
                buffer.append(iter.next());
            }
        }
        return buffer.toString();
    }    
}

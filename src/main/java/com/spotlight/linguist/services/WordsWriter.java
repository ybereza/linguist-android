package com.spotlight.linguist.services;

import java.io.File;
import java.lang.ref.WeakReference;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.spotlight.linguist.R;
import com.spotlight.linguist.utils.FileUtils;

public class WordsWriter extends Service {
    public final static String INTENT_EXTRA_PATH = "com.spotlight.linguist.path";
    private final static String TAG = WordsWriter.class.getSimpleName();

    private static class DelayedWriter extends AsyncTask<String, Integer, FileUtils.WriteStatus> {
        private final File path;
        private final WeakReference<Context> context;

        public DelayedWriter(File path, Context context) {
            this.path = path;
            this.context = new WeakReference<>(context);
        }

        @Override
        protected FileUtils.WriteStatus doInBackground(String... params) {
            return FileUtils.writeNewWordIntoFileAtPath(path, params[0]);
        }

        private String getToastString(FileUtils.WriteStatus st, Context ctx) {
            int resId = R.string.unknown_error;

            switch (st) {
                case E_OK:
                    resId = R.string.word_saved;
                    break;
                case E_FILE_NOT_FOUND:
                    resId = R.string.error_opening_file;
                    break;
                case E_WRITE_ERROR:
                    resId = R.string.can_not_write_to_file;
                    break;
                case E_TO_FEW_PARAMS:
                    resId = R.string.to_few_parameters;
                    break;
            }
            return ctx.getResources().getString(resId);
        }

        @Override
        protected void onPostExecute(FileUtils.WriteStatus st) {
            Context ctx = context.get();
            if (ctx != null) {
                Toast.makeText(ctx, getToastString(st, ctx), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(ctx, WordsWriter.class);
                ctx.stopService(i);
            }
        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "Service is started");
        final String text = intent.getStringExtra(Intent.EXTRA_TEXT);
        final File path = (File)intent.getSerializableExtra(INTENT_EXTRA_PATH);
        DelayedWriter writer = new DelayedWriter(path, this);
        writer.execute(text);

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Service is destroyed");
        super.onDestroy();
    }
}

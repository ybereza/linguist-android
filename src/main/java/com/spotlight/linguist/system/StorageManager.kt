package com.spotlight.linguist.system

import java.io.File

interface StorageManager {
    fun root() : File
    fun cacheDir() : File
    fun lessonsDir() : File

    fun performAsyncFileOperation(file : File, action : (File) -> Unit)
}
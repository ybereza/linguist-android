package com.spotlight.linguist.actions;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.actions.base.MenuAction;
import com.spotlight.linguist.ui.lessons.list.LessonListFragment;

import androidx.fragment.app.FragmentManager;

public class ShowSortMethod extends MenuAction {
    public final static String TAG = ShowSortMethod.class.getSimpleName();
    public final static int ID = R.id.menu_sorttype;

    @Override
    public void run(Activity ctx, MenuItem item) {
        final AppCompatActivity fa = (AppCompatActivity)ctx;
        final FragmentManager fm = fa.getSupportFragmentManager();
        View actionButton = ctx.findViewById(R.id.menu_sorttype);
        PopupMenu popup = new PopupMenu(ctx, actionButton);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.sort_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.sort_by_name:
                        SettingsManager.setSortMethod(SettingsManager.LessonSortMethod.BY_NAME);
                        break;
                    case R.id.sort_by_time:
                        SettingsManager.setSortMethod(SettingsManager.LessonSortMethod.BY_LAST_EDIT_TIME);
                        break;
                }
                LessonListFragment ll = (LessonListFragment)fm.findFragmentByTag(LessonListFragment.TAG);
                if (ll != null) {
                    //TODO: do something to update list
                }
                return true;
            }
        });
        popup.show();
    }
}

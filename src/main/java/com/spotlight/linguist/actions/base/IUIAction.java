package com.spotlight.linguist.actions.base;

import android.app.Activity;

public interface IUIAction {
    public static class IncorrectParameterException extends Exception {
        public IncorrectParameterException(String msg) {
            super(msg);
        }
    }

    public void checkedRun(Activity ctx, Object... objects) throws IncorrectParameterException;
}

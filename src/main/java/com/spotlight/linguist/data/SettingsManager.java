package com.spotlight.linguist.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Environment;
import android.util.Log;

import com.spotlight.linguist.LinguistApp;
import com.spotlight.linguist.R;

import java.io.File;
import java.io.IOException;

public abstract class SettingsManager {
    public static final String TAG = SettingsManager.class.getSimpleName();
    public enum LessonSortMethod {
        BY_NAME,
        BY_LAST_EDIT_TIME
    }

    private final static String DEFAULT_PATH = "Lessons";
    private final static String PATH = "LessonsPath";

    private static boolean mCardsInverted = false;
    private static boolean mRandomOrder = false;
    private static LessonSortMethod mSortMethod = LessonSortMethod.BY_NAME;

    public static File getLessonsPath(Context context) {
        return new File(context.getExternalFilesDir(null), DEFAULT_PATH);
    }

    public static void saveSettingsIntoPrefs(SharedPreferences prefs) {
        SharedPreferences.Editor ed = prefs.edit();
        Resources res = LinguistApp.Companion.getRes();
        ed.putBoolean(res.getString(R.string.inverted_appearance), isCardsInverted());
        ed.putBoolean(res.getString(R.string.random_order), isRandomOrder());
        ed.putString(res.getString(R.string.sort_method), sortMethod().toString());
        ed.apply();
    }

    public static void restoreSettingsFromPrefs(SharedPreferences prefs) {
        Resources res = LinguistApp.Companion.getRes();
        setIsCardsInverted(prefs.getBoolean(
                res.getString(R.string.inverted_appearance),
                false));
        setCardsInRandomOrder(prefs.getBoolean(
                res.getString(R.string.random_order),
                false));
        String method = prefs.getString(
                res.getString(R.string.sort_method),
                LessonSortMethod.BY_NAME.toString());
        setSortMethod(LessonSortMethod.valueOf(method));
    }

    public static LessonSortMethod sortMethod() {
        return mSortMethod;
    }

    public static void setSortMethod(LessonSortMethod method) {
        mSortMethod = method;
    }

    public static boolean isCardsInverted() {
        return mCardsInverted;
    }

    public static boolean isRandomOrder() {
        return mRandomOrder;
    }

    public static void setIsCardsInverted(boolean flag) {
        mCardsInverted = flag;
    }

    public static void setCardsInRandomOrder(boolean flag) {
        mRandomOrder = flag;
    }
}

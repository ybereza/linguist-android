package com.spotlight.linguist.actions.base;

import android.app.Activity;
import android.view.MenuItem;

public abstract class MenuAction implements IUIAction {
    public abstract void run(Activity ctx, MenuItem item);

    public void checkedRun(Activity ctx, Object... objects) throws IncorrectParameterException {
        if (objects.length > 0) {
            Object first = objects[0];
            if (first instanceof MenuItem) {
                run(ctx, (MenuItem) first);
            }
            else {
                throw new IncorrectParameterException("MenuAction expected MenuItem as second parameter but incorrect type was given");
            }
        }
        else {
            throw new IncorrectParameterException("MenuAction expected MenuItem as second parameter but no one was given");
        }
    }
}

package com.spotlight.linguist.ui.main.controllers;


import android.app.Activity;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.spotlight.linguist.R;

public class NavigationController implements NavigationView.OnNavigationItemSelectedListener {
	private final NavigationView mNavigationView;
	private final DrawerLayout mDrawerLayout;
	private OnNavigationActionListener mListener;

	public interface OnNavigationActionListener {
		boolean onNavigationAction(int actionId);
	}

	public NavigationController(DrawerLayout drawer, NavigationView navigationView) {
		mDrawerLayout = drawer;
		mNavigationView = navigationView;
	}

	public void setupNavigationView(Activity act) {
		VectorDrawableCompat addIcon = VectorDrawableCompat.create(act.getResources(),
				R.drawable.ic_add_white_24px, null);
		addIcon.setTint(Color.GRAY);

		VectorDrawableCompat settingsIcon = VectorDrawableCompat.create(act.getResources(),
				R.drawable.ic_settings_white_24px, null);
		settingsIcon.setTint(Color.GRAY);

		Menu menu = mNavigationView.getMenu();
		for (int i = 0; i < menu.size(); i++) {
			MenuItem item = menu.getItem(i);
			switch (item.getItemId()) {
				case R.id.nav_add_deck:
					item.setIcon(addIcon);
					break;
				case R.id.nav_settings:
					item.setIcon(settingsIcon);
					break;
			}
		}

		mNavigationView.setNavigationItemSelectedListener(this);
	}

	public void setNavigationListener(OnNavigationActionListener listener) {
		mListener = listener;
	}

	public void toggleNavigationDrawer() {
		mDrawerLayout.openDrawer(GravityCompat.START);
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
		mDrawerLayout.closeDrawers();
		mNavigationView.postDelayed(new Runnable() {
			@Override
			public void run() {
				item.setChecked(false);
			}
		}, 1000);

		return mListener != null && mListener.onNavigationAction(item.getItemId());
	}
}

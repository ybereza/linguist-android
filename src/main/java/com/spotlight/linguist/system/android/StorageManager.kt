package com.spotlight.linguist.system.android

import android.Manifest
import android.content.Context
import android.os.Environment
import com.spotlight.linguist.system.PermissionProvider
import com.spotlight.linguist.system.StorageManager
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class AndroidStorageManager(private val context: Context,
                            private val permissionProvider : PermissionProvider) : StorageManager {
    override fun root(): File {
        return context.filesDir
    }

    override fun cacheDir(): File {
        return context.cacheDir
    }

    override fun lessonsDir(): File {
        return File(root(), "Lessons")
    }

    private val scheduler : ExecutorService = Executors.newFixedThreadPool(1)

    enum class StorageType {
        INTERNAL_PRIVATE,
        EXTERNAL_PRIVATE,
        EXTERNAL_PUBLIC,
        UNDEFINED
    }

    override fun performAsyncFileOperation(file: File, action: (File) -> Unit) {
        when (file.storageType(context)) {
            StorageType.INTERNAL_PRIVATE -> internalAsyncActionRun(file, action)
            else -> requestWritePermissionAndRun(file, action)
        }
    }

    private fun requestWritePermissionAndRun(file: File, action: (File) -> Unit) {
        if (permissionProvider.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            internalAsyncActionRun(file, action)
        }
        else {
            permissionProvider.requestPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                if (it) internalAsyncActionRun(file, action)
            }
        }
    }

    private fun internalAsyncActionRun(file: File, action: (File) -> Unit) {
        scheduler.submit {
            action(file)
        }
    }
}

internal fun File.storageType(context: Context) : AndroidStorageManager.StorageType {
    val externalPrivate = context.getExternalFilesDir(null)?.absolutePath ?: ""
    val internalPrivate = context.filesDir.absolutePath
    val externalPublic = Environment.getExternalStorageDirectory().absolutePath

    return when {
        absolutePath.startsWith(externalPrivate) -> AndroidStorageManager.StorageType.EXTERNAL_PRIVATE
        absolutePath.startsWith(externalPublic) -> AndroidStorageManager.StorageType.EXTERNAL_PUBLIC
        absolutePath.startsWith(internalPrivate) -> AndroidStorageManager.StorageType.INTERNAL_PRIVATE
        else -> AndroidStorageManager.StorageType.UNDEFINED
    }
}

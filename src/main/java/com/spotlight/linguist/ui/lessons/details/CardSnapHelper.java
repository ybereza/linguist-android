package com.spotlight.linguist.ui.lessons.details;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;


public class CardSnapHelper extends PagerSnapHelper {
    private int snapPosition = RecyclerView.NO_POSITION;
    private final OnSnapListener listener;

    public interface OnSnapListener {
        void onSnapToPosition(int position);
    }

    CardSnapHelper(@NonNull OnSnapListener listener) {
        this.listener = listener;
    }

    @Override
    public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX,
                                      int velocityY) {
        snapPosition = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
        return snapPosition;
    }

    @Override
    public boolean onFling(int velocityX, int velocityY) {
        if (super.onFling(velocityX, velocityY)) {
            if (snapPosition != RecyclerView.NO_POSITION) {
                listener.onSnapToPosition(snapPosition);
            }
            return true;
        }
        return false;
    }
}

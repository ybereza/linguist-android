package com.spotlight.linguist.ui.main.controllers;

import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.spotlight.linguist.R;

/**
 * Copyrigh Mail.ru Games (c) 2015
 * Created by y.bereza.
 */

public class ToolbarController {
	private final ActionBar toolbar;
	private TextView title;

	public ToolbarController(ActionBar toolbar, TextView title) {
		this.toolbar = toolbar;
		this.title = title;
		setupToolBar();
	}

	private void setupToolBar() {
		if (toolbar != null) {
			//toolbar.setHomeButtonEnabled(true);
			//toolbar.setElevation(toolbar.getThemedContext().getResources().getDisplayMetrics().density * 8);
			//toolbar.setDisplayHomeAsUpEnabled(true);
			//toolbar.setHomeAsUpIndicator(R.drawable.ic_menu);
			toolbar.setDisplayShowTitleEnabled(false);
			title.setText(R.string.app_name);
		}
	}

	public void resetHomeIndicator() {
		toolbar.setHomeAsUpIndicator(R.drawable.ic_menu);
	}
}

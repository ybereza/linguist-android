package com.spotlight.linguist.ui.lessons.list

interface LessonListView {
    fun showErrorMessage(stringId: Int)
    fun setAdapter(adapter: LessonsListAdapter)
}
package com.spotlight.linguist.data;

import java.util.ArrayList;
import java.util.List;

import com.spotlight.linguist.utils.StringUtils;
import com.spotlight.linguist.data.Lesson.FileFormatException;

public class Card {
    public static final String STRING_SPLIT_CHAR = ",";
    public static final String TRANSLATION_SPLIT_CHAR = ";";
    private List<String> mBase;
    private List<String> mTranslation;
    private boolean mRotated;

    public Card() {
        mBase = new ArrayList<String>(1);
        mTranslation = new ArrayList<String>(1);
    }

    public Card(String base, String translation) {
        mBase = new ArrayList<String>(1);
        mTranslation = new ArrayList<String>(1);
        mBase.add(base);
        mTranslation.add(translation);
    }
    
    public Card(List<String> base, List<String> translation) {
        mBase = base; mTranslation = translation;
    }

    public static Card createFromString(String text) throws FileFormatException {
        String[] words = text.split(TRANSLATION_SPLIT_CHAR);
        if (words.length > 0) {
            final String base = words[0];
            String translation = "";
            try {
                translation = words[1];
            }
            catch (ArrayIndexOutOfBoundsException e) {
                //there is no translation, but user can add it lately
            }
            final ArrayList<String> bWords = splitWords(base);
            final ArrayList<String> tWords = splitWords(translation);
            
            return new Card(bWords, tWords);
         }
        else {
            throw new FileFormatException(text);
        }
    }
    
    private static ArrayList<String> splitWords(String word) {
        final String[] parts = word.split(STRING_SPLIT_CHAR);
        if (parts != null) {
            final ArrayList<String> arrayOfWords = new ArrayList<String>(parts.length);
            for (String part : parts) {
                arrayOfWords.add(part);
            }
            return arrayOfWords;
        }
        final ArrayList<String> arrayOfWords = new ArrayList<String>(1);
        arrayOfWords.add(word);
        return arrayOfWords;
    }
    
    public List<String> getBaseWords() {
        return mBase;
    }
    
    public List<String> getTranslatedWords() {
        return mTranslation;
    }

    public void addBaseWord(String baseWord) {
        mBase.add(baseWord);
    }

    public void addTranslatedWord(String translation) {
        mTranslation.add(translation);
    }

    public void setBaseWords(List<String> baseWords) {
        mBase = new ArrayList<>(baseWords);
    }

    public void setTranslationWords(List<String> translationWords) {
        mTranslation = new ArrayList<>(translationWords);
    }

    public boolean isRotated() {
        return mRotated;
    }

    public void setRotated(boolean state) {
        mRotated = state;
    }

    @Override
    public String toString() {
        final String base = StringUtils.join(mBase, STRING_SPLIT_CHAR);
        final String translation = StringUtils.join(mTranslation, STRING_SPLIT_CHAR);
        return base + TRANSLATION_SPLIT_CHAR + translation;
    }
}

package com.spotlight.linguist.data;

import com.spotlight.linguist.utils.Randomizer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Lesson {
    public static final String FILE_EXTENSION = "txt";

    public static class FileFormatException extends Exception {
        private static final long serialVersionUID = 1L;
        public FileFormatException(String line) {
            super(line);
        }
    }
    
    private final String mLessonName;
    private String mFilePath;
    private long mTimeStamp;
    private boolean mModified = false;
    private ArrayList<Card> mCards = new ArrayList<Card>();
    
    private Lesson(String lessonName) {
        mLessonName = lessonName;
    }
    
    public static Lesson createFromJson(String jsonString) {
        try {
            JSONTokener json = new JSONTokener(jsonString);
            JSONObject data = (JSONObject)json.nextValue();
            Lesson lesson = new Lesson(data.getString("name"));
            lesson.mTimeStamp = data.getLong("timestamp");
            lesson.mFilePath = data.getString("filepath");
            JSONArray cards = data.getJSONArray("cards");
            final int len = cards.length();
            for (int i = 0; i < len; ++i) {
                final String cardString = cards.getString(i);
                try {
                    lesson.mCards.add(Card.createFromString(cardString));
                }
                catch(FileFormatException e) {
                    e.printStackTrace();
                }
            }
            return lesson;
        }
        catch(JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static Lesson createFromFile(File file) throws FileFormatException, FileNotFoundException {
        final String fileName = file.getName();
        final String lessonName = fileName.substring(0, fileName.indexOf("."));
        final Lesson lesson = new Lesson(lessonName);
        lesson.mTimeStamp = file.lastModified();
        lesson.mFilePath = file.getAbsolutePath();
        final BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        
        try {
            while ((line = reader.readLine()) != null) {
                lesson.addCard(Card.createFromString(line));
            }
        }
        catch(FileFormatException e) {
            throw new FileFormatException(file.getAbsolutePath()+" in line\n"+e.getMessage());
        }
        catch(IOException e) {
            throw new FileFormatException(file.getAbsolutePath()+" is broken and can not be read");
        }
        finally {
            try {
                reader.close();
            }
            catch(IOException e) {
                // pass, nothing can do with this
            }
        }
        return lesson;
    }

    public void saveToFile(File file) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        Iterator<Card> iter = mCards.iterator();
        while (iter.hasNext()) {
            writer.write(iter.next().toString());
            if (iter.hasNext()) {
                writer.newLine();
            }
        }
        writer.flush();
        writer.close();
        mModified = false;
    }

    public void addCard(Card card) {
        mCards.add(card);
        mModified = true;
    }

    public void updateCard(int position, Card card) {
        Card old = getCard(position);
        if (old != null && card != null) {
            old.setBaseWords(card.getBaseWords());
            old.setTranslationWords(card.getTranslatedWords());
            mModified = true;
        }
    }

    public void deleteCard(int cardNum) {
        mCards.remove(cardNum);
        mModified = true;
    }

    public boolean isModified() {
        return mModified;
    }
    
    public String getLessonName() {
        return mLessonName;
    }
    
    public int getCardsNum() {
        return mCards.size();
    }
    
    public Card getCard(int num) {
        return mCards.get(num);
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public Lesson shuffle() {
        if (mCards.size() > 0) {
            ArrayList<Card> shuffled = new ArrayList<Card>(mCards.size());
            Randomizer randomizer = new Randomizer(mCards.size(), false);
            int rnd = 0;
            while ((rnd = randomizer.getNext()) >= 0) {
                shuffled.add(mCards.get(rnd));
            }
            mCards = shuffled;
        }
        return this;
    }

    private String toJson() {
        JSONObject data = new JSONObject();
        try {
            data.put("name", mLessonName);
            data.put("filepath", mFilePath);
            data.put("timestamp", mTimeStamp);
            JSONArray cards = new JSONArray();
            for (Card card : mCards) {
                cards.put(card.toString());
            }
            data.put("cards", cards);
            return data.toString();            
        } catch (JSONException e) {
            e.printStackTrace();            
        }
        return null;
    }
    
    @Override
    public String toString() {
        return toJson();
    }

    public String getFilePath() {
        return mFilePath;
    }
}

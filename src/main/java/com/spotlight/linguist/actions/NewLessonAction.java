package com.spotlight.linguist.actions;

import android.app.Activity;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.actions.base.UIAction;
import com.spotlight.linguist.data.LessonsDataSource;
import com.spotlight.linguist.data.StorageManager;
import com.spotlight.linguist.data.android.AndroidStorageManager;
import com.spotlight.linguist.utils.FileUtils;

/**
 * Copyrigh Mail.ru Games (c) 2015
 * Created by y.bereza.
 */

public class NewLessonAction extends UIAction {
    public static final int ID = R.id.nav_add_deck;
    public NewLessonAction() {
    }
    @Override
    public void run(Activity ctx) {
        //TODO: create new action not dependant on storage manager
    }
}

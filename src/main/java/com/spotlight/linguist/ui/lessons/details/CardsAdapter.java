package com.spotlight.linguist.ui.lessons.details;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.Card;
import com.spotlight.linguist.data.Lesson;
import com.spotlight.linguist.data.SettingsManager;

import java.util.List;

class CardsAdapter extends RecyclerView.Adapter<CardViewHolder> implements CardViewHolder.OnItemsClickListener, Animator.AnimatorListener {

    private @Nullable Lesson lesson;
    private @Nullable ViewGroup animatedView;
    private static final String WORDS_LINEAR_LAYOUT = "WORDS_LINEAR_LAYOUT";

    CardsAdapter() {

    }

    public void setLesson(@Nullable Lesson lesson) {
        this.lesson = lesson;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,
                parent, false);
        return new CardViewHolder(cardLayout, this);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        if (lesson != null) {
            resetViewAtPosition(holder.itemView, position);
            holder.setPosition(position);
        }
    }

    @Override
    public int getItemCount() {
        return lesson != null ? lesson.getCardsNum() : 0;
    }

    private void addSubViews(ViewGroup viewGroup, List<String> words) {
        /* create a main word with larger font size */
        final Context context = viewGroup.getContext();
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setTag(WORDS_LINEAR_LAYOUT);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        ViewGroup.LayoutParams params = viewGroup.getLayoutParams();
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        linearLayout.setLayoutParams(params);

        TextView textView = createNewTextView(context);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
        textView.setText(words.get(0));
        textView.setTag("text");
        linearLayout.addView(textView);
        for (int i = 1; i < words.size(); ++i) {
            /* create a subwords with smaller font size */
            textView = createNewTextView(context);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            textView.setText(words.get(i));
            linearLayout.addView(textView);
        }
        viewGroup.addView(linearLayout);
    }

    private TextView createNewTextView(Context context) {
        TextView textView = new TextView(context);
        textView.setGravity(Gravity.CENTER);
        return textView;
    }

    @Override
    public void onItemClick(CardViewHolder holder, int position) {
        final boolean baseState = SettingsManager.isCardsInverted();
        if (lesson != null) {
            Card card = lesson.getCard(position);
            final boolean nextState = !card.isRotated();
            if (!baseState) {
                List<String> words = nextState ? card.getTranslatedWords() : card.getBaseWords();
                animatedView = (ViewGroup) holder.itemView;
                animatedView.removeAllViews();
                addSubViews(animatedView, words);
            } else {
                List<String> words = nextState ? card.getBaseWords() : card.getTranslatedWords();
                animatedView = (ViewGroup) holder.itemView;
                animatedView.removeAllViews();
                addSubViews(animatedView, words);
            }
            card.setRotated(nextState);
            AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(holder.itemView.getContext(),
                    R.animator.card_flip_left);
            set.setTarget(animatedView);
            set.addListener(this);
            set.setInterpolator(new AccelerateDecelerateInterpolator());
            set.start();
        }
    }

    public void addCard(Card card) {
        if (lesson != null) {
            lesson.addCard(card);
            notifyDataSetChanged();
        }
    }

    public int deleteCardAtPosition(int pos) {
        if (lesson != null) {
            lesson.deleteCard(pos++);
            if (pos < lesson.getCardsNum()) {
                return pos;
            }
        }
        return -1;
    }

    public @Nullable Card getCardAtPosition(int pos) {
        return lesson != null ? lesson.getCard(pos) : null;
    }

    public void updateCardAtPos(Card card, int pos) {
        if (card != null) {
            lesson.updateCard(pos, card);
        }
    }

    @Override
    public void onAnimationStart(Animator animation) {
        if (animatedView != null) {
            View v = animatedView.findViewWithTag(WORDS_LINEAR_LAYOUT);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (animatedView != null) {
            View v = animatedView.findViewWithTag(WORDS_LINEAR_LAYOUT);
            if (v != null) {
                v.setVisibility(View.VISIBLE);
            }
            animatedView = null;
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        if (animatedView != null) {
            View v = animatedView.findViewWithTag(WORDS_LINEAR_LAYOUT);
            if (v != null) {
                v.setVisibility(View.VISIBLE);
            }
            animatedView = null;
        }
    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }

    public void resetViewAtPosition(View v, int position) {
        ViewGroup cardView = (ViewGroup)v;

        Card card = lesson != null ? lesson.getCard(position) : null;
        if (card != null) {
            card.setRotated(false);
            List<String> words = !SettingsManager.isCardsInverted() ? card.getBaseWords() : card.getTranslatedWords();
            cardView.removeAllViews();
            addSubViews(cardView, words);
        }
    }
}

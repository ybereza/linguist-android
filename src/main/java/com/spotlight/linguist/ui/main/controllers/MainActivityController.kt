package com.spotlight.linguist.ui.main.controllers

import android.os.Bundle

import com.spotlight.linguist.ui.main.LinguistMainActivity
import com.spotlight.linguist.actions.ActionsRunner
import com.spotlight.linguist.ui.main.NavigationRouter


class MainActivityController(
        private var activity: LinguistMainActivity) : NavigationController.OnNavigationActionListener {

    private var router = NavigationRouter(activity)

    var isFirstStart = true
        private set

    fun restoreState(oldState: Bundle?) {
        isFirstStart = oldState?.getBoolean("FirstStart") ?: false;
    }

    fun saveState(state: Bundle) {
        state.putBoolean("FirstStart", isFirstStart)
    }

    override fun onNavigationAction(actionId: Int): Boolean {
        return ActionsRunner.getInstance().executeUIAction(activity, actionId)
    }

    fun setTwoPaneMode(enabled: Boolean) {
        router.setTwoPaneMode(enabled)
    }

    fun showLessonsList() {
        router.navigateToLessonsList()
    }

    fun goToSettings() {
        router.navigateToSettings()
    }

    fun onDestroy() {
    }
}

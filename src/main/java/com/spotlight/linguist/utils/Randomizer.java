package com.spotlight.linguist.utils;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by ybereza on 19.05.13.
 */
public class Randomizer {
    private final Random randomizer = new Random();
    private int lastPosition;
    private final ArrayList<Integer> randoms;
    private final boolean infinite;
    private boolean done = false;
    /**
     * Inits Randomizer
     * @param maxVal contains a maximum number that will be returned by randomizer
     */
    public Randomizer(int maxVal, boolean infinite) {
        this.infinite = infinite;
        lastPosition = maxVal - 1;
        randoms = new ArrayList<Integer>(maxVal);

        for (int i = 0; i < maxVal; ++i) {
            randoms.add(i,i);
        }
    }

    public int getNext() {
        int idx = 0;
        int rnd = 0;

        if (done) {
            return -1;
        }

        if (lastPosition > 0) {
            idx = Math.abs(randomizer.nextInt()) % lastPosition;
        }
        rnd = randoms.get(idx);
        randoms.set(idx, randoms.get(lastPosition));
        randoms.set(lastPosition, rnd);

        if (lastPosition > 0) {
            --lastPosition;
        }
        else if (infinite) {
            lastPosition = randoms.size() - 1;
        }
        else {
            done = true;
        }
        return rnd;
    }
}

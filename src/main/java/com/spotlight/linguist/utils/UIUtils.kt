package com.spotlight.linguist.utils

import androidx.appcompat.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.util.TypedValue

import com.spotlight.linguist.R

object UIUtils {

    interface DialogCancelationCallback {
        fun onDialogClosed()
    }

    fun convertDptoPx(ctx: Context, dp: Int): Int {
        val r = ctx.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).toInt()
    }

    fun CreateReportDialog(ctx: Context, stringId: Int, callback: DialogCancelationCallback?): Dialog {
        return CreateReportDialog(ctx, ctx.getString(stringId), callback)
    }

    fun CreateReportDialog(ctx: Context, stringId: Int): Dialog {
        return CreateReportDialog(ctx, ctx.getString(stringId), null)
    }

    @JvmOverloads
    fun CreateReportDialog(ctx: Context, str: String, callback: DialogCancelationCallback? = null): Dialog {
        val builder = AlertDialog.Builder(ctx)
        builder.setMessage(str)
        builder.setPositiveButton(R.string.close) { dialog, which ->
            dialog.cancel()
            callback?.onDialogClosed()
        }

        return builder.create()
    }
}

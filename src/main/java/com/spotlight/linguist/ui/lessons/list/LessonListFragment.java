package com.spotlight.linguist.ui.lessons.list;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.actions.ShowSortMethod;
import com.spotlight.linguist.data.StorageManager;
import com.spotlight.linguist.data.android.AndroidStorageManager;
import com.spotlight.linguist.system.PermissionProvider;
import com.spotlight.linguist.ui.main.NavigationRouter;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class LessonListFragment extends Fragment implements LessonListView, PermissionProvider {
    public static final String TAG = LessonListFragment.class.getSimpleName();

    private RecyclerView lessonsListView;
    private FloatingActionButton fab;
    private LessonListController controller;
    private StorageManager storageManager;
    private NavigationRouter router;
    @Override
    public void showErrorMessage(int stringId) {

    }

    @Override
    public boolean hasPermission(@NotNull String permission) {
        return false;
    }

    @Override
    public void requestPermission(@NotNull String permission, @NotNull Function1<? super Boolean, Unit> result) {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_lesson_list, container);
        lessonsListView = root.findViewById(R.id.list);
        fab = root.findViewById(R.id.fab);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        controller.onViewCreated();
    }

    @Override
    public void onStart() {
        super.onStart();
        controller.onStart(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        router = new NavigationRouter((AppCompatActivity) requireActivity());
        storageManager = new AndroidStorageManager(requireContext(), this, false);
        controller = new LessonListController(router, storageManager);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        controller.onStop();
    }

    public void setAdapter(LessonsListAdapter adapter) {
        lessonsListView.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.main_menu, menu);
        MenuItem item = menu.findItem(ShowSortMethod.ID);
        if (item != null) {
            item.setTitle(SettingsManager.sortMethod() == SettingsManager.LessonSortMethod.BY_NAME ?
                    getString(R.string.sort_by_time) :
                    getString(R.string.sort_by_name));
            VectorDrawableCompat icon = VectorDrawableCompat.create(getContext().getResources(),
                    R.drawable.ic_sort_white_24px, null);
            icon.setTint(Color.WHITE);
            item.setIcon(icon);
        }
    }
}

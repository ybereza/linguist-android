package com.spotlight.linguist.utils;

import java.util.Collection;

/**
 * Created by ybereza on 3/1/14.
 */
public abstract class CollectionUtils {
    public interface Transformer<T,U> {
        public U transform(T value);
    }

    public static <T,U> Collection<U> transform(Collection<T> in, Collection<U> out, Transformer<T,U> transformer) {
        for (T value: in) {
            out.add(transformer.transform(value));
        }
        return out;
    }
}

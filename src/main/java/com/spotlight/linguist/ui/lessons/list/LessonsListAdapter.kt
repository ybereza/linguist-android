package com.spotlight.linguist.ui.lessons.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.spotlight.linguist.R

class LessonsListAdapter : RecyclerView.Adapter<LessonsListAdapter.LessonListItemViewHolder>() {

    private var lessons: ArrayList<LessonEntry> = ArrayList();

    fun addAll(lessonNames: Collection<String>) {

    }

    fun clear() {
        lessons = ArrayList()
    }

    data class LessonEntry(val name: String)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LessonListItemViewHolder {
        return LessonListItemViewHolder(View.inflate(parent.context, R.layout.lesson_list_item, null))
    }

    override fun onBindViewHolder(holder: LessonListItemViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 0
    }

    fun getLessonName(position: Int): String {
        return lessons[position].name;
    }

    class LessonListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}

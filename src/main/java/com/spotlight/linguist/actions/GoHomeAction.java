package com.spotlight.linguist.actions;

import android.app.Activity;

import com.spotlight.linguist.ui.main.LinguistMainActivity;
import com.spotlight.linguist.actions.base.IUIAction;

public class GoHomeAction implements IUIAction {
    public final static String TAG = GoHomeAction.class.getSimpleName();
    public final static int ID = android.R.id.home;

    @Override
    public void checkedRun(Activity ctx, Object... items) throws IncorrectParameterException{
        if (ctx instanceof LinguistMainActivity) {
            LinguistMainActivity activity = (LinguistMainActivity)ctx;
            if (activity.getFragmentManager().getBackStackEntryCount() > 0) {
                activity.getFragmentManager().popBackStackImmediate();
                //stack is empty, hide "back" button
                if (activity.getFragmentManager().getBackStackEntryCount() == 0) {
                    activity.getToolbarController().resetHomeIndicator();
                }
            }
        }
        else {
            throw new IncorrectParameterException("This action requires LinguistMainActivity");
        }
    }
}

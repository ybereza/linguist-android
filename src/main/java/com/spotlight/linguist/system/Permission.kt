package com.spotlight.linguist.system

interface PermissionProvider {
    fun hasPermission(permission: String) : Boolean
    fun requestPermission(permission: String, result : (Boolean) -> Unit )
}
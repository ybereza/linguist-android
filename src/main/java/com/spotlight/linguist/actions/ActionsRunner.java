package com.spotlight.linguist.actions;

import android.app.Activity;
import android.util.SparseArray;

import com.spotlight.linguist.R;
import com.spotlight.linguist.actions.base.IUIAction;
import com.spotlight.linguist.utils.UIUtils;

public class ActionsRunner {
	private static volatile ActionsRunner sSelf;
	private final SparseArray<IUIAction> mMenuActions = new SparseArray<>();

	public static ActionsRunner getInstance() {
		if (sSelf == null) {
			synchronized (ActionsRunner.class) {
				if (sSelf == null) {
					sSelf = new ActionsRunner();
				}
			}
		}
		return sSelf;
	}

	private ActionsRunner() {
		initMenuActions();
	}

	private void initMenuActions() {
		mMenuActions.put(GoHomeAction.ID, new GoHomeAction());
		mMenuActions.put(GoToPrefsAction.ID, new GoToPrefsAction());
		mMenuActions.put(ShowSortMethod.ID, new ShowSortMethod());
		mMenuActions.put(NewLessonAction.ID, new NewLessonAction());
	}

	private void showMessage(Activity act, int msgId) {
		UIUtils.INSTANCE.CreateReportDialog(act, msgId).show();
	}

	private void showMessage(Activity act, String msg) {
		UIUtils.INSTANCE.CreateReportDialog(act, msg).show();
	}


	public boolean executeUIAction(Activity act, int id, Object... items) {
		try {
			IUIAction ma = mMenuActions.get(id);
			if (ma != null) {
				ma.checkedRun(act, items);
				return true;
			}
		}
		catch (IUIAction.IncorrectParameterException e) {
			String errMsg = act.getString(R.string.can_not_exec_action);
            showMessage(act, errMsg + " " + id);
		}
		return false;
	}

}

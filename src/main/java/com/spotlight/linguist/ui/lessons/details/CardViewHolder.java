package com.spotlight.linguist.ui.lessons.details;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;


public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    interface OnItemsClickListener {
        void onItemClick(CardViewHolder view, int position);
    }

    private final OnItemsClickListener listener;
    private int position;

    CardViewHolder(View itemView, OnItemsClickListener clickListener) {
        super(itemView);

        this.listener = clickListener;
        this.itemView.setOnClickListener(this);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public void onClick(View v) {
        this.listener.onItemClick(this, this.position);
    }
}

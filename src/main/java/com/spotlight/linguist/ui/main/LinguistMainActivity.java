package com.spotlight.linguist.ui.main;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.actions.ActionsRunner;
import com.spotlight.linguist.ui.main.controllers.MainActivityController;
import com.spotlight.linguist.ui.main.controllers.ToolbarController;

public class LinguistMainActivity extends AppCompatActivity {

    private final static String TAG = LinguistMainActivity.class.getSimpleName();

    private ToolbarController toolbarController;
    private MainActivityController activityController;

    private NavigationRouter router;

    //AppCompat Vector Drawables hack
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityController = new MainActivityController(this);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbarController = new ToolbarController(getSupportActionBar(), (TextView) toolbar.findViewById(R.id.app_title));

        getMainActivityController().restoreState(savedInstanceState);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SettingsManager.restoreSettingsFromPrefs(prefs);

        if (getMainActivityController().isFirstStart()) {
            if (findViewById(R.id.lesson_detail_fragment) != null) {
                getMainActivityController().setTwoPaneMode(true);
            }
            else {
                getMainActivityController().setTwoPaneMode(false);
                getMainActivityController().showLessonsList();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SettingsManager.saveSettingsIntoPrefs(prefs);
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        getMainActivityController().saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStackImmediate();
            //stack is empty, hide "back" button
            if (getFragmentManager().getBackStackEntryCount() == 0) {
                toolbarController.resetHomeIndicator();
            }
        }
        else {
            super.onBackPressed();
        }
    }

    public ToolbarController getToolbarController() {
        return toolbarController;
    }

    public MainActivityController getMainActivityController() {
        return activityController;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean called = super.onOptionsItemSelected(item);
        if (!called) {
            called = ActionsRunner.getInstance().executeUIAction(this, item.getItemId(), item);
        }
        return called;
    }
}

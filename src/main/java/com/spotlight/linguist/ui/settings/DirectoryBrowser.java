package com.spotlight.linguist.ui.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import com.spotlight.linguist.R;

public class DirectoryBrowser extends AppCompatActivity {
    public static final String SELECTED_DIR = "SELECTED_DIR";
    public static final String INITIAL_DIR = "INITIAL_DIR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent i = getIntent();
        String initialDir = i != null ? i.getStringExtra(INITIAL_DIR) : null ;

        DirectoryBrowserFragment fragment = new DirectoryBrowserFragment();
        if (initialDir != null) {
            fragment.setArguments(i.getExtras());
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.main_fragment_container, fragment, DirectoryBrowserFragment.TAG);
        transaction.commit();
    }
}

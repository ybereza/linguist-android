package com.spotlight.linguist.ui.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.spotlight.linguist.R;
import com.spotlight.linguist.data.StorageManager;
import com.spotlight.linguist.data.android.AndroidStorageManager;
import com.spotlight.linguist.system.PermissionProvider;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class DirectoryBrowserFragment extends Fragment implements PermissionProvider {
    public static final String TAG = DirectoryBrowserFragment.class.getSimpleName();

    private Button mSelectButton;
    private ListView mDirectoryList;
    private ArrayAdapter<String> mDirectoryAdapter;
    private String mSelectedDir;
    private Pattern mPrevDirPattern = Pattern.compile("(\\S+)([/][^/]+[/]?$)");
    private StorageManager mStorageManager;

    public String getSelectedDir() {
        return mSelectedDir;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final String text = mDirectoryAdapter.getItem(position);
            if (text.equalsIgnoreCase("..")) {
                goDirUp();
            }
            else {
                goToDir(mSelectedDir + File.separator + text);
            }
        }
    };

    private View.OnClickListener mOnSelectButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent();
            i.putExtra(DirectoryBrowser.SELECTED_DIR, mSelectedDir);
            getActivity().setResult(Activity.RESULT_OK, i);
            getActivity().finish();
        }
    };

    private void goToDir(String dir) {
        mSelectedDir = dir;
        if (mDirectoryAdapter != null) {
            mDirectoryAdapter.clear();
            mDirectoryAdapter.addAll(scanDir(mSelectedDir));
            mDirectoryAdapter.notifyDataSetChanged();
        }
    }

    private boolean goDirUp() {
        Matcher m = mPrevDirPattern.matcher(mSelectedDir);
        if (m.matches()) {
            goToDir(m.group(1));
            return true;
        }
        return false;
    }

    private boolean canGoUp(String dir) {
        return mPrevDirPattern.matcher(dir).matches();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStorageManager = new AndroidStorageManager(requireContext(), this, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.browser, null);
        mSelectButton = (Button)rootView.findViewById(R.id.select);
        mSelectButton.setOnClickListener(mOnSelectButtonListener);
        mDirectoryList = (ListView)rootView.findViewById(R.id.directory_list);
        mDirectoryList.setOnItemClickListener(mOnItemClickListener);
        mDirectoryAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_activated_1, android.R.id.text1);
        mDirectoryList.setAdapter(mDirectoryAdapter);

        final Bundle args = getArguments();
        final String defaultInitialDir = mStorageManager.lessonsDir().getAbsolutePath();
        final String initialDir = args != null ?
                args.getString(DirectoryBrowser.INITIAL_DIR, defaultInitialDir) : defaultInitialDir;
        goToDir(initialDir);

        return rootView;
    }

    private ArrayList<String> scanDir(String dir) {
        final File curDir = new File(dir);
        if (curDir.exists()) {
            FileFilter directoryFilter = new FileFilter() {
                public boolean accept(File file) {
                    return file.isDirectory();
                }
            };

            final File dirs[] = curDir.listFiles(directoryFilter);
            final ArrayList<String> subDirs = new ArrayList<>((dirs != null ? dirs.length : 0) + 1);
            if (canGoUp(dir)) {
                subDirs.add("..");
            }
            if (dirs != null) {
                for (File d : dirs) {
                    subDirs.add(d.getName());
                }
            }
            return subDirs;
        }
        return new ArrayList<>();
    }

    @Override
    public boolean hasPermission(@NotNull String permission) {
        return false;
    }

    @Override
    public void requestPermission(@NotNull String permission, @NotNull Function1<? super Boolean, Unit> result) {

    }
}

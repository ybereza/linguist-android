package com.spotlight.linguist.utils;

import com.spotlight.linguist.data.Card;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Copyrigh Mail.ru Games (c) 2015
 * Created by y.bereza.
 */

public class FileUtils {
    public enum WriteStatus {
        E_OK,
        E_FILE_NOT_FOUND,
        E_WRITE_ERROR,
        E_TO_FEW_PARAMS
    }

    public static void createDirectoriesIfNeeded(File path) {
        String[] parts = path.getAbsolutePath().split(File.separator);
        String part = "";
        for(int i = 0; i < parts.length - 1; ++i) {
            if (!parts[i].equals("")) {
                part = part + File.separator + parts[i];
                File f = new File(part);
                f.mkdir();
            }
        }
    }

    public static boolean generateNewLessonFile(File path) {
        try {
            createDirectoriesIfNeeded(path);
            File textFile = new File(path + File.separator + generateFileName());
            return textFile.createNewFile();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static WriteStatus writeNewWordIntoFileAtPath(File path, String text) {
        try {
            createDirectoriesIfNeeded(path);
            File textFile = new File(path + File.separator + generateFileName());
            try {
                final boolean appendNewLine = textFile.exists(); //if file exists with words, first append new line
                FileOutputStream stream = new FileOutputStream(textFile, true);
                PrintWriter printer = new PrintWriter(stream, true);
                if (appendNewLine) {
                    printer.println();
                }
                printer.println(text.trim()+ Card.TRANSLATION_SPLIT_CHAR);
                printer.close();
                stream.close();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
                return WriteStatus.E_FILE_NOT_FOUND;
            }
            catch (IOException e) {
                e.printStackTrace();
                return WriteStatus.E_WRITE_ERROR;
            }
        }
        catch (IndexOutOfBoundsException e) {
            return WriteStatus.E_TO_FEW_PARAMS;
        }
        return WriteStatus.E_OK;
    }

    public static String generateFileName() {
        final Calendar cal = Calendar.getInstance();
        final Date date = cal.getTime();
        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy'.txt'", new Locale("ru", "RU", ""));
        String result = dateFormat.format(date);
        return result;
    }

}

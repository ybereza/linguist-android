package com.spotlight.linguist.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;

import com.spotlight.linguist.data.SettingsManager;
import com.spotlight.linguist.data.StorageManager;
import com.spotlight.linguist.data.android.AndroidStorageManager;
import com.spotlight.linguist.system.PermissionProvider;

import org.jetbrains.annotations.NotNull;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class SendActionReceiver extends AppCompatActivity implements PermissionProvider {

    private StorageManager storageManager = new AndroidStorageManager(this, this, false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        proceedIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        proceedIntent(intent);
    }

    private void proceedIntent(Intent intent) {
        final String action = intent.getAction();
        if (action !=null && action.equals(Intent.ACTION_SEND)) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            SettingsManager.restoreSettingsFromPrefs(prefs);
            Intent writeWord = new Intent(this, WordsWriter.class);
            writeWord.putExtra(Intent.EXTRA_TEXT, intent.getStringExtra(Intent.EXTRA_TEXT));
            writeWord.putExtra(WordsWriter.INTENT_EXTRA_PATH, storageManager.lessonsDir());
            startService(writeWord);
            finish();
        }
    }

    @Override
    public boolean hasPermission(@NotNull String permission) {
        return false;
    }

    @Override
    public void requestPermission(@NotNull String permission, @NotNull Function1<? super Boolean, Unit> result) {

    }
}
